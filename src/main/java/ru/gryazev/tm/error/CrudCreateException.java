package ru.gryazev.tm.error;

public final class CrudCreateException extends RuntimeException {

    public CrudCreateException() {
        super("Error during create operation");
    }

}
