package ru.gryazev.tm.error;

public final class CrudListEmptyException extends RuntimeException {

    public CrudListEmptyException() {
        super("Error: list is empty");
    }

}
