package ru.gryazev.tm.error;

public final class CrudUpdateException extends RuntimeException{

    public CrudUpdateException() {
        super("Error during update operation");
    }

}
