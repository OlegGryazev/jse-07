package ru.gryazev.tm.enumerated;

public enum RoleType {

    USER("User"),
    ADMIN("Administrator");

    private final String roleName;

    RoleType(final String roleName) {
        this.roleName = roleName;
    }

    public String displayName(){
        return roleName;
    }

}
