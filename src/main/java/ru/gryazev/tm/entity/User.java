package ru.gryazev.tm.entity;

import ru.gryazev.tm.enumerated.RoleType;

public final class User extends AbstractCrudEntity {

    private String login = "";

    private String pwdHash = "";

    private RoleType roleType;

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(final String pwdHash) {
        this.pwdHash = pwdHash;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(final RoleType roleType) {
        this.roleType = roleType;
    }

    public User() {
        setUserId(getId());
    }

    @Override
    public String toString() {
        return "Login: " + login + "\n" +
                "Role: " + roleType;
    }

}
