package ru.gryazev.tm.entity;

import java.util.UUID;

public abstract class AbstractCrudEntity {

    private String id = UUID.randomUUID().toString();

    private String userId = null;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
