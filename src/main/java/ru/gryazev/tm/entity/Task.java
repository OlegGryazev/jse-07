package ru.gryazev.tm.entity;

import ru.gryazev.tm.util.DateUtils;

import java.util.Date;

public final class Task extends AbstractCrudEntity {

    private String userId = null;

    private String projectId = null;

    private String name = "";

    private String details = "";

    private Date dateStart;

    private Date dateFinish;

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(final String details) {
        this.details = details;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                        "Details: %s\n" +
                        "Starts: %s\n" +
                        "Ends: %s",
                name,
                details,
                DateUtils.formatDateToString(dateStart),
                DateUtils.formatDateToString(dateFinish));
    }

}
