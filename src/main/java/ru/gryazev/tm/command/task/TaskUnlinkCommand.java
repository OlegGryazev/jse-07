package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

public final class TaskUnlinkCommand extends AbstractCommand {

    public TaskUnlinkCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-unlink";
    }

    @Override
    public String getDescription() {
        return "Unlink selected task.";
    }

    @Override
    public void execute() {
        final StateService stateService = serviceLocator.getStateService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final String userId = stateService.getCurrentUserId();
        final String currentProjectId = stateService.getCurrentProjectId();
        final int taskIndex = consoleView.getTaskIndex();
        final String taskId = taskService.getTaskId(currentProjectId, userId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        final Task unlinkedTask = taskService.unlinkTask(userId, taskService.findOne(userId, taskId));
        if (unlinkedTask == null) throw new CrudUpdateException();
        consoleView.print("[OK]");
    }

}
