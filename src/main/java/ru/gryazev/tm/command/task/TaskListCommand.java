package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    public TaskListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of selected project.";
    }

    @Override
    public void execute() {
        final StateService stateService = serviceLocator.getStateService();
        final String projectId = stateService.getCurrentProjectId();
        final List<Task> tasks = serviceLocator.getTaskService().listTaskByProject(stateService.getCurrentUserId(), projectId);
        if (tasks == null || tasks.size() == 0) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            consoleView.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
