package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudDeleteException;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

public final class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task from selected project.";
    }

    @Override
    public void execute() {
        final ITaskService taskService = serviceLocator.getTaskService();
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        final int taskIndex = consoleView.getTaskIndex();
        final String taskId = taskService.getTaskId(getProjectId(userId), userId, taskIndex);

        final Task removedTask = taskService.remove(userId, taskId);
        if (removedTask == null) throw new CrudDeleteException();
        consoleView.print("[DELETED]");
    }

    private String getProjectId(String userId) {
        final StringBuilder projectId = new StringBuilder(serviceLocator.getStateService().getCurrentProjectId());
        if (projectId.toString().isEmpty()){
            final int projectIndex = consoleView.getProjectIndex();
            projectId.append(serviceLocator.getProjectService().getProjectId(projectIndex, userId));
        }
        if (projectId.toString().isEmpty()) throw new CrudNotFoundException();
        return projectId.toString();
    }

}
