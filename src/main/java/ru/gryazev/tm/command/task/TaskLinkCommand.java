package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class TaskLinkCommand extends AbstractCommand {

    public TaskLinkCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link selected task to project.";
    }

    @Override
    public void execute() {
        final StateService stateService = serviceLocator.getStateService();
        final String userId = stateService.getCurrentUserId();
        final String currentProjectId = stateService.getCurrentProjectId();
        final int projectIndex = consoleView.getProjectIndex();
        final String projectId = serviceLocator.getProjectService()
                .getProjectId(projectIndex, stateService.getCurrentUserId());
        final int taskIndex = consoleView.getTaskIndex();

        final Task linkedTask = serviceLocator.getTaskService().linkTask(userId, currentProjectId, projectId, taskIndex);
        if (linkedTask == null) throw new CrudUpdateException();
        consoleView.print("[OK]");
    }

}
