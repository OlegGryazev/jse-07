package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;
import ru.gryazev.tm.view.ConsoleView;

import java.util.List;

public final class TaskListUnlinkedCommand extends AbstractCommand {

    public TaskListUnlinkedCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-list-unlinked";
    }

    @Override
    public String getDescription() {
        return "Shows all unlinked tasks.";
    }

    @Override
    public void execute() {
        final List<Task> tasks = serviceLocator.getTaskService().listTaskUnlinked(serviceLocator.getStateService().getCurrentUserId());
        if (tasks == null || tasks.size() == 0) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            consoleView.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
