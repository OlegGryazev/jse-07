package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task at selected project.";
    }

    @Override
    public void execute() throws IOException {
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        consoleView.print("[TASK CREATE]");

        final Task task = consoleView.getTaskFromConsole();
        task.setUserId(userId);
        task.setProjectId(getProjectId(userId));
        final Task createdTask = serviceLocator.getTaskService().create(userId, task);
        if (createdTask == null) throw new CrudCreateException();
        consoleView.print("[OK]");
    }

    private String getProjectId(String userId) {
        final StringBuilder projectId = new StringBuilder(serviceLocator.getStateService().getCurrentProjectId());
        if (projectId.toString().isEmpty()){
            final int projectIndex = consoleView.getProjectIndex();
            projectId.append(serviceLocator.getProjectService().getProjectId(projectIndex, userId));
        }
        if (projectId.toString().isEmpty()) throw new CrudNotFoundException();
        return projectId.toString();
    }

}
