package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws IOException {
        final ITaskService taskService = serviceLocator.getTaskService();
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        final String projectId = getProjectId(userId);
        final int taskIndex = consoleView.getTaskIndex();
        final String taskId = taskService.getTaskId(projectId, userId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        final Task task = consoleView.getTaskFromConsole();
        task.setId(taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
        final Task editedTask = taskService.edit(userId, task);
        if (editedTask == null) throw new CrudUpdateException();
        consoleView.print("[OK]");
    }

    private String getProjectId(String userId) {
        final StringBuilder projectId = new StringBuilder(serviceLocator.getStateService().getCurrentProjectId());
        if (projectId.toString().isEmpty()){
            final int projectIndex = consoleView.getProjectIndex();
            projectId.append(serviceLocator.getProjectService().getProjectId(projectIndex, userId));
        }
        if (projectId.toString().isEmpty()) throw new CrudNotFoundException();
        return projectId.toString();
    }

}
