package ru.gryazev.tm.command.task;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Clear tasks list.";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().clear(serviceLocator.getStateService().getCurrentUserId());
        consoleView.print("[CLEAR OK]");
    }

}
