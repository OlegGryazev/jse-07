package ru.gryazev.tm.command;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public abstract class AbstractCommand {

    protected final ServiceLocator serviceLocator;

    protected ConsoleView consoleView;

    private boolean allowed;

    public AbstractCommand(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.allowed = false;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException;

    public boolean isAllowed() {
        return allowed;
    }

    protected void setAllowed() {
        this.allowed = true;
    }

    public RoleType[] getRoles() {
        return null;
    }

    public void setConsoleView(final ConsoleView consoleView) {
        this.consoleView = consoleView;
    }

}
