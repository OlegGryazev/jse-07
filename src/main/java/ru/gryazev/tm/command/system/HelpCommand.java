package ru.gryazev.tm.command.system;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.view.ConsoleView;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
        setAllowed();
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        serviceLocator.getStateService().getCommands().forEach((k, v) -> consoleView.print(v.getName() + ": " + v.getDescription()));
    }

}
