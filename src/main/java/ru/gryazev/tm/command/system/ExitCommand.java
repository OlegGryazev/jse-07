package ru.gryazev.tm.command.system;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
        setAllowed();
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from program.";
    }

    @Override
    public void execute() {
        consoleView.close();
        System.exit(0);
    }

}
