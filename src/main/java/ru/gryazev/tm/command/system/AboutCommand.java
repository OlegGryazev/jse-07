package ru.gryazev.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class AboutCommand extends AbstractCommand {

    public AboutCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
        setAllowed();
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Shows information about build.";
    }

    @Override
    public void execute() {
        consoleView.print("Created by: " + Manifests.read("Created-By"));
        consoleView.print("Version: " + Manifests.read("buildNumber"));
    }

}
