package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;

public final class UserViewCommand extends AbstractCommand {

    public UserViewCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-view";
    }

    @Override
    public String getDescription() {
        return "View user data.";
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getStateService().getCurrentUserId();
        final User user = serviceLocator.getUserService().findOne(currentUserId, currentUserId);
        if (user != null) consoleView.print(user.toString());
    }

}
