package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import java.io.IOException;

public class UserListCommand extends AbstractCommand {

    public UserListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Shows all users (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() {
        for (final User user : serviceLocator.getUserService().findAll())
            consoleView.print(user.getLogin());
    }

    @Override
    public RoleType[] getRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
