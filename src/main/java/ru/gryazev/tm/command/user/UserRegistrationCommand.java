package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
        setAllowed();
    }

    @Override
    public String getName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Registration of new user.";
    }

    @Override
    public void execute() throws IOException {
        final User user = consoleView.getUserPwdRepeat();
        if (user == null) return;

        final User createdUser = serviceLocator.getUserService().create(user.getUserId(), user);
        if (createdUser == null) throw new CrudCreateException();
        consoleView.print("[OK]");
    }

}
