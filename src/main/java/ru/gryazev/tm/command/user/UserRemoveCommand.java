package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public class UserRemoveCommand extends AbstractCommand {

    public UserRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected user (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        final int userIndex = consoleView.getUserIndex();
        final String userId = userService.getUserId(userIndex);
        if (userId == null) throw new CrudNotFoundException();
        userService.remove(userId, userId);
        consoleView.print("[OK]");
    }

    @Override
    public RoleType[] getRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
