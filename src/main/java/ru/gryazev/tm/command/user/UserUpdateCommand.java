package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class UserUpdateCommand extends AbstractCommand {

    public UserUpdateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        final String pwd = consoleView.getPwdHashFromConsole();
        final String pwdRepeat = consoleView.getPwdHashFromConsole();
        if (!pwd.equals(pwdRepeat)){
            consoleView.print("Entered passwords do not match!");
            return;
        }
        final String currentUserId = serviceLocator.getStateService().getCurrentUserId();

        final User user = userService.findOne(currentUserId, currentUserId);
        if (user == null) throw new CrudNotFoundException();
        user.setPwdHash(pwd);
        final User editedUser = userService.edit(currentUserId, user);
        if (editedUser == null) throw new CrudUpdateException();
        consoleView.print("[OK]");
    }

}
