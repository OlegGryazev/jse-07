package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.service.StateService;

public final class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() {
        final StateService stateService = serviceLocator.getStateService();
        if (stateService.isUserLogged()) {
            stateService.setCurrentUserId(null);
            consoleView.print("You are logged out.");
        }
    }

}
