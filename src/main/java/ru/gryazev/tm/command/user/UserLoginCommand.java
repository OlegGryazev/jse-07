package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
        setAllowed();
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() throws IOException {
        final User user = consoleView.getUserFromConsole();
        final String userId = serviceLocator.getUserService().login(user);
        if (userId == null) {
            consoleView.print("[ERROR DURING USER LOGIN]");
            return;
        }
        consoleView.print("[" + user.getLogin() + " logged in]");
        serviceLocator.getStateService().setCurrentUserId(userId);
    }

}
