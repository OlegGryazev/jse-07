package ru.gryazev.tm.command.user;

import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class UserEditCommand extends AbstractCommand {

    public UserEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user data.";
    }

    @Override
    public void execute() throws IOException {
        final IUserService userService = serviceLocator.getUserService();
        consoleView.print("[USER EDIT]");
        final User userEditData = consoleView.getUserPwdRepeat();
        if (userEditData == null) return;
        final String currentUserId = serviceLocator.getStateService().getCurrentUserId();

        final User currentUser = userService.findOne(currentUserId, currentUserId);
        userEditData.setRoleType(currentUser.getRoleType());
        userEditData.setId(currentUser.getId());
        final User editedUser = userService.edit(currentUser.getUserId(), userEditData);
        if (editedUser == null) throw new CrudUpdateException();
        consoleView.print("[OK]");
    }

}
