package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Clear projects list.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        serviceLocator.getProjectService().clear(userId);
        serviceLocator.getTaskService().clear(userId);
        consoleView.print("[CLEAR OK]");
    }

}
