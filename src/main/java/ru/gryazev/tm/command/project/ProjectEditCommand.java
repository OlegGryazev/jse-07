package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws IOException {
        final StateService stateService = serviceLocator.getStateService();
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = stateService.getCurrentUserId();
        final int projectIndex = consoleView.getProjectIndex();
        final String projectId = projectService.getProjectId(projectIndex, stateService.getCurrentUserId());
        if (projectId == null) throw new CrudNotFoundException();

        final Project project = consoleView.getProjectFromConsole();
        project.setId(projectId);
        project.setUserId(userId);
        if (projectService.edit(userId, project) == null) throw new CrudUpdateException();
        consoleView.print("[OK]");
    }

}
