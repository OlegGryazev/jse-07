package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        consoleView.print("[PROJECT CREATE]");

        final Project project = consoleView.getProjectFromConsole();
        project.setUserId(userId);
        final Project createdProject = serviceLocator.getProjectService().create(userId, project);
        if (createdProject == null) throw new CrudCreateException();
        consoleView.print("[OK]");
    }

}
