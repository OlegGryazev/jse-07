package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.view.ConsoleView;

public final class ProjectSelectCommand extends AbstractCommand {

    public ProjectSelectCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Select project. To reset, type \"-1\".";
    }

    @Override
    public void execute() {
        final StateService stateService = serviceLocator.getStateService();
        final int projectIndex = consoleView.getProjectIndex();
        final String projectId = serviceLocator.getProjectService()
                .getProjectId(projectIndex, stateService.getCurrentUserId());
        stateService.setCurrentProjectId(projectId);
    }

}
