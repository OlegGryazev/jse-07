package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudListEmptyException;
import ru.gryazev.tm.view.ConsoleView;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        final List<Project> projects = serviceLocator.getProjectService().findByUserId(serviceLocator.getStateService().getCurrentUserId());
        if (projects == null || projects.size() == 0) throw new CrudListEmptyException();
        for (int i = 0; i < projects.size(); i++)
            consoleView.print((i + 1) + ". " + projects.get(i).getName());
    }

}
