package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.view.ConsoleView;

public final class ProjectViewCommand extends AbstractCommand {

    public ProjectViewCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-view";
    }

    @Override
    public String getDescription() {
        return "View selected project.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        final int projectIndex = consoleView.getProjectIndex();
        final String projectId = projectService.getProjectId(projectIndex, userId);

        final Project project = projectService.findOne(userId, projectId);
        if (project == null) throw new CrudNotFoundException();
        consoleView.print(project.toString());
    }

}
