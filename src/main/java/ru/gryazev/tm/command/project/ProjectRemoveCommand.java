package ru.gryazev.tm.command.project;

import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudDeleteException;
import ru.gryazev.tm.view.ConsoleView;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        final String userId = serviceLocator.getStateService().getCurrentUserId();
        final int projectIndex = consoleView.getProjectIndex();
        final String projectId = projectService.getProjectId(projectIndex, userId);

        final Project removedProject = projectService.remove(userId, projectId);
        if (removedProject == null) throw new CrudDeleteException();
        serviceLocator.getTaskService().removeByProjectId(userId, projectId);
        consoleView.print("[DELETED]");
    }

}
