package ru.gryazev.tm.util;

import java.util.List;

public final class ArrayUtils {

    public static boolean indexExists(final List list, final int index){
        return index >= 0 && index < list.size();
    }

}
