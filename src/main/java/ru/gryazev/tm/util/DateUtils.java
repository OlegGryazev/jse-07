package ru.gryazev.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtils {

    public static Date inputDate(final BufferedReader in){
        final SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date result = null;
        try {
            result = format.parse(in.readLine());
        } catch (ParseException | IOException e){
            System.out.println("Entered date is incorrect. You can edit date later.");
        }
        return result;
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

    public static String formatDateToString(final Date date){
        return date != null ? dateFormat.format(date) : "undefined";
    }

}
