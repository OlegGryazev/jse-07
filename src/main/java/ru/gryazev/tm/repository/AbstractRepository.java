package ru.gryazev.tm.repository;

import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractCrudEntity> implements IRepository<T> {

    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public T findOne(final String userId, final String id) {
        final T t = entities.get(id);
        if (t == null || !t.getUserId().equals(userId)) return null;
        return t;
    }

    @Override
    public T merge(final String userId, final T t) {
        if (!t.getUserId().equals(userId)) return null;
        entities.put(t.getId(), t);
        return t;
    }

    @Override
    public Map<String, T> findAll(final String userId) {
        final Map<String, T> result = new LinkedHashMap<String , T>();
        entities.forEach((k, v) -> {
            if (v.getUserId().equals(userId)) result.put(v.getId(), v);
        });
        return result;
    }

    @Override
    public T persist(final String userId, final T t) {
        if (!t.getUserId().equals(userId)) return null;
        if (findOne(userId, t.getId()) != null) return null;
        entities.put(t.getId(), t);
        return t;
    }

    @Override
    public T remove(final String userId, final String id) {
        if (!entities.get(id).getUserId().equals(userId)) return null;
        return entities.remove(id);
    }

    @Override
    public void removeAll(final String userId) {
        final List<T> tsToRemove = entities.values().stream().filter(o ->
                o.getUserId().equals(userId)).collect(Collectors.toList());
        tsToRemove.forEach(o -> entities.remove(o.getId()));
    }

}
