package ru.gryazev.tm.repository;

import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findByUserIdAndName(final String userId, final String projectName) {
        return entities.values().stream().filter(o ->
                userId.equals(o.getUserId()) && projectName.equals(o.getName()))
                .findFirst().orElse(null);
    }

}
