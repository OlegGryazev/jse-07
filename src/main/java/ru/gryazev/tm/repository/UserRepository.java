package ru.gryazev.tm.repository;

import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public User findByLoginAndPwd(final String login, final String pwd) {
        return entities.values().stream().filter(o ->
                o.getLogin().equals(login) && o.getPwdHash().equals(pwd)).findFirst().orElse(null);
    }

    public List<User> findAll() {
        return new ArrayList<User>(entities.values());
    }

}
