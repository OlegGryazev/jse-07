package ru.gryazev.tm.repository;

import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.entity.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public List<Task> findTasksByProjectId(final String userId, final String projectId) {
        return entities.values().stream().filter(o ->
                o.getProjectId() != null && o.getProjectId().equals(projectId) && o.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

}
