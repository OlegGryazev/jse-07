package ru.gryazev.tm.service;

import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.util.ArrayUtils;

import java.util.Arrays;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository repository;

    public UserService(final IUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean isEntityValid(final User user) {
        if (user == null || user.getRoleType() == null) return false;
        return isValid(user.getId(), user.getLogin(), user.getPwdHash());
    }

    public String login(final User user) {
        if (!isEntityValid(user)) return null;
        final User loggedUser = repository.findByLoginAndPwd(user.getLogin(), user.getPwdHash());
        if (loggedUser == null) return null;
        return loggedUser.getId();
    }

    public boolean checkRole(final String userId, final RoleType[] roles) {
        if (roles == null || !isValid(userId)) return false;
        return Arrays.asList(roles).contains(findOne(userId, userId).getRoleType());
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public String getUserId(int userIndex) {
        final List<User> users = findAll();
        if (users == null || !ArrayUtils.indexExists(users, userIndex)) return null;
        return users.get(userIndex).getId();
    }

    @Override
    public IRepository<User> getRepository() {
        return repository;
    }

}
