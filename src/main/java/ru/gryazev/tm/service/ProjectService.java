package ru.gryazev.tm.service;

import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.util.ArrayUtils;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository repository;

    public ProjectService(final IProjectRepository repository) {
        this.repository = repository;
    }

    public String getProjectId(final int projectIndex, final String userId){
        if (!isValid(userId)) return null;
        final List<Project> projects = findByUserId(userId);
        if (projects == null || !ArrayUtils.indexExists(projects, projectIndex)) return null;
        return projects.get(projectIndex).getId();
    }

    @Override
    public boolean isEntityValid(final Project project) {
        if (project == null) return false;
        return isValid(project.getId(), project.getName(), project.getUserId());
    }

    @Override
    public Project findByName(final String userId, final String projectName) {
        if (!isValid(userId, projectName)) return null;
        return repository.findByUserIdAndName(userId, projectName);
    }

    @Override
    public IRepository<Project> getRepository() {
        return repository;
    }

}
