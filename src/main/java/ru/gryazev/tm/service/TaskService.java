package ru.gryazev.tm.service;

import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.util.ArrayUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository repository;

    public TaskService(final ITaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean isEntityValid(final Task task) {
        if (task == null) return false;
        return isValid(task.getId(), task.getUserId(), task.getProjectId(), task.getName());
    }

    @Override
    public List<Task> listTaskUnlinked(final String userId) {
        if (!isValid(userId)) return null;
        return findByUserId(userId).stream().filter(o ->
                o.getProjectId() == null).collect(Collectors.toList());
    }

    @Override
    public void removeByProjectId(final String userId, final String projectId) {
        for (final Map.Entry<String, Task> pair : repository.findAll(userId).entrySet())
            if (pair.getValue().getProjectId().equals(projectId))
                repository.remove(userId, pair.getValue().getId());
    }

    @Override
    public Task unlinkTask(final String userId, final Task task) {
        if (!isEntityValid(task) || !isValid(userId)) return null;
        task.setProjectId(null);
        return repository.merge(userId, task);
    }

    @Override
    public String getTaskId(final String projectId, final String userId, final int taskIndex) {
        if (!isValid(projectId, userId)) return null;
        final List<Task> tasks = repository.findTasksByProjectId(userId, projectId);
        if (tasks == null || !ArrayUtils.indexExists(tasks, taskIndex)) return null;
        return tasks.get(taskIndex).getId();
    }

    @Override
    public IRepository<Task> getRepository() {
        return repository;
    }

    @Override
    public List<Task> listTaskByProject(final String userId, final String projectId) {
        if (!isValid(userId, projectId)) return null;
        return repository.findTasksByProjectId(userId, projectId);
    }

    @Override
    public Task linkTask(final String userId, final String oldProjectId, final String newProjectId, final int taskIndex) {
        if (!isValid(userId, newProjectId)) return null;
        final List<Task> tasks = oldProjectId == null ?
                listTaskUnlinked(userId) : listTaskByProject(userId, oldProjectId);
        if (tasks == null || !ArrayUtils.indexExists(tasks, taskIndex)) return null;
        final Task task = tasks.get(taskIndex);
        if (task == null) return null;
        task.setProjectId(newProjectId);
        return repository.merge(userId, task);
    }

}