package ru.gryazev.tm.service;

import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.service.IService;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractService <T extends AbstractCrudEntity> implements IService<T> {

    @Override
    public T create(final String userId, final T t) {
        if (!isEntityValid(t) || !isValid(userId)) return null;
        return getRepository().persist(userId, t);
    }

    @Override
    public T findOne(final String userId, final String entityId) {
        if (!isValid(userId, entityId)) return null;
        return getRepository().findOne(userId, entityId);
    }

    @Override
    public List<T> findByUserId(final String userId) {
        if (!isValid(userId)) return null;
        return new ArrayList<T>(getRepository().findAll(userId).values());
    }

    @Override
    public T edit(final String userId, final T t) {
        if (!isEntityValid(t) || !isValid(userId)) return null;
        return getRepository().merge(userId, t);
    }

    @Override
    public T remove(final String userId, final String entityId) {
        if (!isValid(userId,entityId)) return null;
        return getRepository().remove(userId, entityId);
    }

    @Override
    public void clear(final String userId){
        if (!isValid(userId)) return;
        getRepository().removeAll(userId);
    }

    public boolean isValid(final String... strings){
        for (final String str : strings)
            if (str == null || str.isEmpty()) return false;
        return true;
    }

    public abstract boolean isEntityValid(final T t);

    public abstract IRepository<T> getRepository();

}
