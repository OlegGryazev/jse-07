package ru.gryazev.tm.service;

import ru.gryazev.tm.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.Map;

public final class StateService {

    private String currentUserId = null;

    private String currentProjectId = null;

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public String getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(final String currentUserId) {
        this.currentUserId = currentUserId;
    }

    public String getCurrentProjectId() {
        return currentProjectId;
    }

    public void setCurrentProjectId(final String currentProjectId) {
        this.currentProjectId = currentProjectId;
    }

    public boolean isUserLogged(){
        return currentUserId != null;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void setCommands(final Map<String, AbstractCommand> commands) {
        this.commands = commands;
    }

}
