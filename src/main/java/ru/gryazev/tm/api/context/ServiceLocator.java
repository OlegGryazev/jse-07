package ru.gryazev.tm.api.context;

import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.service.StateService;

public interface ServiceLocator {

    public IProjectService getProjectService();

    public ITaskService getTaskService();

    public IUserService getUserService();

    public StateService getStateService();

}
