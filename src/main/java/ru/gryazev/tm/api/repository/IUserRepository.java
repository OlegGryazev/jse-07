package ru.gryazev.tm.api.repository;

import ru.gryazev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    public User findByLoginAndPwd(final String login, final String pwd);

    public List<User> findAll();

}
