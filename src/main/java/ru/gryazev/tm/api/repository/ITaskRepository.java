package ru.gryazev.tm.api.repository;

import ru.gryazev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    public List<Task> findTasksByProjectId(final String userId, final String projectId);

}
