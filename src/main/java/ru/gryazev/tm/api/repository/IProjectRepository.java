package ru.gryazev.tm.api.repository;

import ru.gryazev.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

    public Project findByUserIdAndName(final String userId, final String projectName);

}
