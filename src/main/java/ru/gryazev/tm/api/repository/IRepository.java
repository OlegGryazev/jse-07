package ru.gryazev.tm.api.repository;

import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.Map;

public interface IRepository<T extends AbstractCrudEntity> {

    public T findOne(final String userId, final String id);

    public T merge(final String userId, final T t);

    public Map<String, T> findAll(final String userId);

    public T persist(final String userId, final T t);

    public T remove(final String userId, final String id);

    public void removeAll(final String userId);

}
