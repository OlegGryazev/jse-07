package ru.gryazev.tm.api.service;

import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.List;

public interface IService<T extends AbstractCrudEntity> {

    public T create(final String userId, final T t);

    public T findOne(final String userId, final String entityId);

    public List<T> findByUserId(final String userId);

    public T edit(final String userId, final T t);

    public T remove(final String userId, final String entityId);

    public void clear(final String userId);

    public boolean isValid(final String... strings);

    public IRepository<T> getRepository();

}
