package ru.gryazev.tm.api.service;

import ru.gryazev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    public List<Task> listTaskUnlinked(final String userId);

    public void removeByProjectId(final String userId, final String projectId);

    public Task unlinkTask(final String userId, final Task task);

    public String getTaskId(final String projectId, final String userId, final int taskIndex);

    public List<Task> listTaskByProject(final String userId, final String projectId);

    public Task linkTask(final String userId, final String oldProjectId, final String newProjectId, final int taskIndex);

}
