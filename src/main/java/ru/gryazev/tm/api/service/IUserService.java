package ru.gryazev.tm.api.service;

import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import java.util.List;

public interface IUserService extends IService<User> {

    public String login(final User user);

    public boolean checkRole(final String userId, final RoleType[] roles);

    public List<User> findAll();

    public String getUserId(final int userIndex);

}
