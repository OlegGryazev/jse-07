package ru.gryazev.tm.api.service;

import ru.gryazev.tm.entity.Project;

public interface IProjectService extends IService<Project> {

    public String getProjectId(final int projectIndex, final String userId);

    public Project findByName(final String userId, final String projectName);

}
