package ru.gryazev.tm.context;

import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.command.project.*;
import ru.gryazev.tm.command.system.AboutCommand;
import ru.gryazev.tm.command.system.ExitCommand;
import ru.gryazev.tm.command.system.HelpCommand;
import ru.gryazev.tm.command.task.*;
import ru.gryazev.tm.command.user.*;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.StateService;
import ru.gryazev.tm.service.TaskService;
import ru.gryazev.tm.service.UserService;
import ru.gryazev.tm.view.ConsoleView;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final ConsoleView consoleView = new ConsoleView();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final StateService stateService = new StateService();

    public void init() {
        consoleView.print("**** Welcome to Project Manager ****");
        commandsInit();
        usersInit();
        while (true) {
            try {
                final AbstractCommand command = stateService.getCommands().get(consoleView.readCommand());
                if (command == null) {
                    consoleView.print("Command not found!");
                    continue;
                }
                final String currentUserId = stateService.getCurrentUserId();
                final RoleType[] roles = command.getRoles();
                final boolean checkAccess = stateService.isUserLogged() || command.isAllowed();
                final boolean checkRole = command.getRoles() == null || userService.checkRole(currentUserId, roles);
                if (!checkAccess || !checkRole) {
                    consoleView.print("Access denied!");
                    continue;
                }
                command.execute();
            } catch (RuntimeException e) {
                consoleView.print(e.getMessage());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void commandsInit() {
        final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
        final AbstractCommand[] commandArray = new AbstractCommand[]{new ExitCommand(this), new HelpCommand(this), new AboutCommand(this),
                new ProjectClearCommand(this), new ProjectCreateCommand(this), new ProjectEditCommand(this), new ProjectListCommand(this),
                new ProjectRemoveCommand(this), new ProjectSelectCommand(this), new ProjectViewCommand(this), new TaskClearCommand(this),
                new TaskClearCommand(this), new TaskCreateCommand(this), new TaskEditCommand(this), new TaskListCommand(this),
                new TaskRemoveCommand(this), new TaskViewCommand(this), new TaskUnlinkCommand(this), new TaskLinkCommand(this),
                new TaskListUnlinkedCommand(this), new UserLoginCommand(this), new UserLogoutCommand(this),
                new UserRegistrationCommand(this), new UserUpdateCommand(this), new UserViewCommand(this), new UserEditCommand(this),
                new UserRemoveCommand(this), new UserListCommand(this)};

        for (final AbstractCommand abstractCommand : commandArray) {
            abstractCommand.setConsoleView(consoleView);
            commands.put(abstractCommand.getName(), abstractCommand);
        }
        stateService.setCommands(commands);
    }

    private void usersInit() {
            final User user = new User();
            user.setLogin("user");
            user.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
            user.setRoleType(RoleType.USER);
            userService.create(user.getId(), user);

            final User admin = new User();
            admin.setLogin("admin");
            admin.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
            admin.setRoleType(RoleType.ADMIN);
            userService.create(admin.getId(), admin);
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public StateService getStateService() {
        return stateService;
    }

    public ConsoleView getConsoleView() {
        return consoleView;
    }

}
