package ru.gryazev.tm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.service.TaskService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class TaskTest extends Assert {

    private final Bootstrap bootstrap = new Bootstrap();

    private final Project project = new Project();

    private final Task task = new Task();

    private final String userId = UUID.randomUUID().toString();

    private final String projectId = UUID.randomUUID().toString();

    @Before
    public void setProject() {
        project.setName("test");
        project.setDetails("details");

        SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date startDate = null;
        try {
            startDate = format.parse("02.02.2020");
        } catch (ParseException e){}

        if (startDate != null)
            project.setDateStart(startDate);

        Date endDate = null;
        try {
            endDate = format.parse("adfasdf");
        } catch (ParseException e){}
        if (endDate != null)
            project.setDateFinish(endDate);
    }

    @Before
    public void setTask() {
        task.setName("test-task");
        task.setDetails("details");

        SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date startDate = null;
        try {
            startDate = format.parse("02.02.2020");
        } catch (ParseException e){}

        if (startDate != null)
            task.setDateStart(startDate);

        Date endDate = null;
        try {
            endDate = format.parse("adfasdf");
        } catch (ParseException e){}
        if (endDate != null)
            task.setDateFinish(endDate);
    }

    @Test
    public void TaskTest() throws IOException {
        ITaskService ts =  bootstrap.getTaskService();

        task.setUserId(userId);
        task.setProjectId(projectId);
        assertNotNull(ts.create(task.getUserId(), task));

        //попытка создать новый таск с userId == null
        task.setUserId(null);
        task.setProjectId(projectId);
        assertNull(ts.create(task.getUserId(), task));

        //попытка создать таск с несовпадающим userId
        task.setUserId(userId);
        task.setProjectId(projectId);
        assertNull(ts.create(UUID.randomUUID().toString(), task));

        assertEquals(1, ts.listTaskByProject(userId, projectId).size());

        //попытка создать таск с повторным id
        task.setUserId(userId);
        task.setProjectId(projectId);
        assertNull(ts.create(userId, task));
        assertEquals(1, ts.listTaskByProject(userId, projectId).size());

        //отвязка таска от проекта и проверка list
        assertNotNull(ts.unlinkTask(userId, task));
        assertEquals(0, ts.listTaskByProject(projectId, userId).size());
        assertEquals(1, ts.listTaskUnlinked(userId).size());
        assertNotNull(ts.findOne(userId, task.getId()));
    }

}
